  layui.config({
     base: CXL.ctxPath + '/layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index' //主入口模块
  }).use(['index', 'form', 'laydate'], function(){
	  var form = layui.form;
	  var admin = layui.admin;
	  var laydate = layui.laydate;
	  var $ = layui.$;

      laydate.render({
          elem: '#biz-pet-birthday-date',
          type: 'datetime',
		  format: 'yyyy-MM-dd HH:mm:ss'
      });

	  //监听提交
	  form.on('submit(form-btn-save)', function(data){
		  var field = data.field;

	      var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引  
	      
	      //提交 Ajax 成功后，关闭当前弹层并重载表格
	      $.ajax({
	             type: "post",
	             url: CXL.ctxPath + '/biz/pet/save',
	             data: field,
	             success: function(res) {
		    		 if(res.success) {
		    			 CXL.success(res.msg);
		    			 parent.layui.table.reload('biz-pet-table'); //重载表格
		    		     parent.layer.close(index); //再执行关闭 
		    		 }else {
		    			 CXL.warn(res.msg);
		    		 }
		    	 }
	         });
	      
	     
     
	  });
	    
	    
  })