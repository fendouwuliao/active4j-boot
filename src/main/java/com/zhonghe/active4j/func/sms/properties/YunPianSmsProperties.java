package com.zhonghe.active4j.func.sms.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 
 * @title YunPianSmsProperties.java
 * @description 
		  云片参数配置
 * @time  2019年12月16日 上午10:39:16
 * @author 刘爽
 * @version 1.0
 */
@Configuration
@ConfigurationProperties(prefix = "func.sms.yunpian")
@Data
public class YunPianSmsProperties {

	private String apikey;
	
	
}
