package com.zhonghe.active4j.biz.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.zhonghe.active4j.biz.entity.PetEntity;
import com.zhonghe.active4j.biz.service.PetService;
import com.zhonghe.active4j.biz.wrapper.PetWrapper;
import com.zhonghe.active4j.core.annotation.Log;
import com.zhonghe.active4j.core.model.AjaxJson;
import com.zhonghe.active4j.core.model.LogType;
import com.zhonghe.active4j.core.model.PageInfo;
import com.zhonghe.active4j.core.query.QueryUtils;
import com.zhonghe.active4j.core.util.MyBeanUtils;
import com.zhonghe.active4j.core.util.ResponseUtil;
import com.zhonghe.active4j.core.util.ShiroUtils;
import com.zhonghe.active4j.system.entity.SysDepartEntity;
import com.zhonghe.active4j.system.entity.SysRoleEntity;
import com.zhonghe.active4j.system.entity.SysUserEntity;
import com.zhonghe.active4j.system.service.SysDepartService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;

/**
 *
 * 活体管理 宠物管理
 */
@Controller
@RequestMapping("/biz/pet")
@Slf4j
public class PetController {

    @Autowired
    private PetService petService;
    @Autowired
    private SysDepartService sysDepartService;

    /**
     * 跳转到列表页面
     * @param model
     * @return
     */
    @RequestMapping("/list")
    public String list(Model model) {
        return "biz/pet/pet_list.html";
    }


    /**
     *  获取表格数据 树形结构
     */
    @RequestMapping("/datagrid")
    @ResponseBody
    public void datagrid(PetEntity petEntity, PageInfo<PetEntity> page, HttpServletRequest request, HttpServletResponse response) {
        //拼接查询条件
        QueryWrapper<PetEntity> queryWrapper = QueryUtils.installQueryWrapper(petEntity, request.getParameterMap());

        //执行查询
        IPage<PetEntity> lstResult = petService.page(page.getPageEntity(), queryWrapper);

        //结果处理,直接写到客户端
        ResponseUtil.write(response, new PetWrapper(lstResult).wrap());
    }

    /**
     * 跳转到页面
     * @param petEntity
     * @param model
     * @return
     */
    @RequestMapping("/add")
    public String add(PetEntity petEntity, Model model) {

        if(StringUtils.isNotEmpty(petEntity.getId())) {
            PetEntity pet = petService.getById(petEntity.getId());
            model.addAttribute("pet", pet);

            //所属部门
            SysDepartEntity dept = sysDepartService.getById(pet.getDeptId());
            model.addAttribute("deptName", dept.getName());
        }

        return "biz/pet/pet.html";
    }

    /**
     * 保存方法
     */
    @RequestMapping("/save")
    @RequiresPermissions("biz:pet:save")
    @ResponseBody
    @Log(type = LogType.save, name = "保存宠物信息", memo = "新增或编辑保存了宠物信息")
    public AjaxJson save(PetEntity petEntity)  {
        AjaxJson j = new AjaxJson();

        try{
            //后端校验
            if(StringUtils.isEmpty(petEntity.getPetNo())) {
                j.setSuccess(false);
                j.setMsg("宠物编号不能为空");
                return j;
            }
            if(StringUtils.isEmpty(petEntity.getPetName())) {
                j.setSuccess(false);
                j.setMsg("宠物名称不能为空");
                return j;
            }
            if(StringUtils.isEmpty(petEntity.getBreed())) {
                j.setSuccess(false);
                j.setMsg("品种分类不能为空");
                return j;
            }
            if(StringUtils.isEmpty(petEntity.getBizKind())) {
                j.setSuccess(false);
                j.setMsg("宠物商业分类不能为空");
                return j;
            }
            if(StringUtils.isEmpty(petEntity.getSex())) {
                j.setSuccess(false);
                j.setMsg("宠物性别不能为空");
                return j;
            }
            if(StringUtils.isEmpty(petEntity.getDeptId())) {
                j.setSuccess(false);
                j.setMsg("所属部门不能为空");
                return j;
            }
            if(petEntity.getBirthday() == null) {
                j.setSuccess(false);
                j.setMsg("出生日期不能为空");
                return j;
            }

            if(StringUtils.isEmpty(petEntity.getSaleStatus())) {
                // 默认售卖状态： 0正常
                petEntity.setSaleStatus("0");
            }

            //新增保存
            if(StringUtils.isEmpty(petEntity.getId())) {
                petEntity.setUpdateDate(new Date());
                petEntity.setUpdateName(ShiroUtils.getSessionUser().getUserName());
                petService.save(petEntity);
            }
            //编辑保存
            else {
                PetEntity tmp = petService.getById(petEntity.getId());
                MyBeanUtils.copyBeanNotNull2Bean(petEntity, tmp);
                tmp.setUpdateDate(new Date());
                tmp.setUpdateName(ShiroUtils.getSessionUser().getUserName());
                petService.saveOrUpdatePet(tmp);
            }

        }catch(Exception e) {
            log.error("保存宠物信息报错，错误信息:" + e.getMessage());
            j.setSuccess(false);
            j.setMsg("系统出错");
            e.printStackTrace();
        }

        return j;
    }


    /**
     *  删除操作
     */
    @RequestMapping("/delete")
    @RequiresPermissions("biz:pet:delete")
    @ResponseBody
    @Log(type = LogType.del, name = "删除宠物信息", memo = "删除了宠物信息")
    public AjaxJson delete(PetEntity petEntity) {
        AjaxJson j = new AjaxJson();
        try {
            petService.delete(petEntity);
        }catch(Exception e) {
            log.error("删除宠物信息出错，错误信息:" + e.getMessage() );
            j.setSuccess(false);
            j.setMsg("系统出错");
            e.printStackTrace();
        }
        return j;
    }


}
