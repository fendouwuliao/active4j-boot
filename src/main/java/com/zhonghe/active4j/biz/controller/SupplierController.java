package com.zhonghe.active4j.biz.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.zhonghe.active4j.biz.entity.PetEntity;
import com.zhonghe.active4j.biz.entity.SupplierEntity;
import com.zhonghe.active4j.biz.service.SupplierService;
import com.zhonghe.active4j.biz.wrapper.PetWrapper;
import com.zhonghe.active4j.biz.wrapper.SupplierWrapper;
import com.zhonghe.active4j.core.annotation.Log;
import com.zhonghe.active4j.core.model.AjaxJson;
import com.zhonghe.active4j.core.model.LogType;
import com.zhonghe.active4j.core.model.PageInfo;
import com.zhonghe.active4j.core.query.QueryUtils;
import com.zhonghe.active4j.core.util.MyBeanUtils;
import com.zhonghe.active4j.core.util.ResponseUtil;
import com.zhonghe.active4j.core.util.ShiroUtils;
import com.zhonghe.active4j.system.entity.SysDepartEntity;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 供应商管理
 */
@Controller
@RequestMapping("/biz/supplier")
@Slf4j
public class SupplierController {

    @Autowired
    private SupplierService supplierSevice;

    /**
     * 跳转到列表页面
     *
     * @param model
     * @return
     */
    @RequestMapping("/list")
    public String list(Model model) {
        return "biz/supplier/supplier_list.html";
    }


    /**
     * 获取表格数据 树形结构
     */
    @RequestMapping("/datagrid")
    @ResponseBody
    public void datagrid(SupplierEntity supplierEntity, PageInfo<SupplierEntity> page, HttpServletRequest request, HttpServletResponse response) {
        //拼接查询条件
        QueryWrapper<SupplierEntity> queryWrapper = QueryUtils.installQueryWrapper(supplierEntity, request.getParameterMap());

        //执行查询
        IPage<SupplierEntity> lstResult = supplierSevice.page(page.getPageEntity(), queryWrapper);

        //结果处理,直接写到客户端
        ResponseUtil.write(response, new SupplierWrapper(lstResult).wrap());
    }

    /**
     * 跳转到页面
     */
    @RequestMapping("/add")
    public String add(SupplierEntity supplierEntity, Model model) {

        if (StringUtils.isNotEmpty(supplierEntity.getId())) {
            SupplierEntity supplier = supplierSevice.getById(supplierEntity.getId());
            model.addAttribute("supplier", supplier);
        } else {
            SupplierEntity supplier = new SupplierEntity();
            supplier.setYiFuPay(BigDecimal.ZERO);
            supplier.setYingFuPay(BigDecimal.ZERO);
            model.addAttribute("supplier", supplier);
        }

        return "biz/supplier/supplier.html";
    }

    /**
     * 保存方法
     */
    @RequestMapping("/save")
    @RequiresPermissions("biz:supplier:save")
    @ResponseBody
    @Log(type = LogType.save, name = "保存供应商信息", memo = "新增或编辑保存了供应商信息")
    public AjaxJson save(SupplierEntity supplierEntity) {
        AjaxJson j = new AjaxJson();

        try {
            //后端校验
            if (StringUtils.isEmpty(supplierEntity.getSupplierName())) {
                j.setSuccess(false);
                j.setMsg("供应商名称不能为空");
                return j;
            }
            if (StringUtils.isEmpty(supplierEntity.getContacts())) {
                j.setSuccess(false);
                j.setMsg("供应商联系人不能为空");
                return j;
            }
            if (StringUtils.isEmpty(supplierEntity.getMobile())) {
                j.setSuccess(false);
                j.setMsg("供应商手机号不能为空");
                return j;
            }
            if (StringUtils.isEmpty(supplierEntity.getStatus())) {
                // 供应商状态： 0正常
                supplierEntity.setStatus("0");
            }

            //新增保存
            if (StringUtils.isEmpty(supplierEntity.getId())) {
                supplierEntity.setUpdateDate(new Date());
                supplierEntity.setUpdateName(ShiroUtils.getSessionUser().getUserName());
                supplierSevice.save(supplierEntity);
            }
            //编辑保存
            else {
                SupplierEntity tmp = supplierSevice.getById(supplierEntity.getId());
                MyBeanUtils.copyBeanNotNull2Bean(supplierEntity, tmp);
                tmp.setUpdateDate(new Date());
                tmp.setUpdateName(ShiroUtils.getSessionUser().getUserName());
                supplierSevice.saveOrUpdatePet(tmp);
            }

        } catch (Exception e) {
            log.error("保存供应商信息报错，错误信息:" + e.getMessage());
            j.setSuccess(false);
            j.setMsg("系统出错");
            e.printStackTrace();
        }

        return j;
    }

    /**
     *  删除操作
     */
    @RequestMapping("/delete")
    @RequiresPermissions("biz:supplier:delete")
    @ResponseBody
    @Log(type = LogType.del, name = "删除供应商信息", memo = "删除了供应商信息")
    public AjaxJson delete(SupplierEntity supplierEntity) {
        AjaxJson j = new AjaxJson();
        try {
            supplierSevice.delete(supplierEntity);
        }catch(Exception e) {
            log.error("删除供应商信息出错，错误信息:" + e.getMessage() );
            j.setSuccess(false);
            j.setMsg("系统出错");
            e.printStackTrace();
        }
        return j;
    }

}
