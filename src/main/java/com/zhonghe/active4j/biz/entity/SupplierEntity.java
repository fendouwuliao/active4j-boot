package com.zhonghe.active4j.biz.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zhonghe.active4j.common.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * 供应商
 */
@TableName("biz_supplier")
@Getter
@Setter
public class SupplierEntity extends BaseEntity {

    /**
     * 供应商名称
     */
    @TableField("SUPPLIER_NAME")
    private String supplierName;

    /**
     * 供应商联系人
     */
    @TableField("CONTACTS")
    private String contacts;

    /**
     * 供应商手机号
     */
    @TableField("MOBILE")
    private String mobile;

    /**
     * 供应商座机号
     */
    @TableField("PHONE")
    private String phone;

    /**
     * 供应商电子邮箱
     */
    @TableField("EMAIL")
    private String email;

    /**
     * 供应商微信号
     */
    @TableField("WECHAT")
    private String wechat;

    /**
     * 供应商QQ号
     */
    @TableField("QQ")
    private String qq;

    /**
     * 供应商地址
     */
    @TableField("ADDRESS")
    private String address;

    /**
     * 供应商纳税号
     */
    @TableField("TAX_NUM")
    private String taxNum;

    /**
     * 供应商银行开户行
     */
    @TableField("BANK_NAME")
    private String bankName;

    /**
     * 供应商纳银行账号
     */
    @TableField("ACCOUNT_NUMBER")
    private String accountNumber;

    /**
     * 供应商状态 0：正常 1：待定
     */
    @TableField("STATUS")
    private String status;

    /**
     * 供应商备注
     */
    @TableField("MEMO")
    private String memo;

    /**
     * 向供应商总的已付款项
     */
    @TableField("YIFUPAY")
    private BigDecimal yiFuPay;
    @TableField(exist = false)
    private String yiFuPayStr;
    /**
     * 向应商总的应付款项
     */
    @TableField("YINGFUPAY")
    private BigDecimal yingFuPay;
    @TableField(exist = false)
    private String yingFuPayStr;

}
