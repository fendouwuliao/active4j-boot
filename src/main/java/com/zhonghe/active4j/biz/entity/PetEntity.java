package com.zhonghe.active4j.biz.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zhonghe.active4j.common.entity.BaseEntity;
import com.zhonghe.active4j.core.annotation.QueryField;
import com.zhonghe.active4j.core.model.QueryCondition;
import com.zhonghe.active4j.core.util.DateUtils;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;


/**
 * 活体管理 宠物管理
 *
 */
@TableName("biz_pet")
@Getter
@Setter
public class PetEntity extends BaseEntity {

	private static final long serialVersionUID = 7348020772533478062L;

	/**
	 * 宠物姓名
	 */
	@TableField("PET_NAME")
	@QueryField(queryColumn="PET_NAME", condition=QueryCondition.like)
	private String petName;
	
	/**
	 * 头像
	 */
	@TableField("HEAD_IMG_URL")
	private String headImgUrl;


	/**
	 * 宠物状态
	 */
	@TableField("HEALTH_STATUS")
	@QueryField(queryColumn="HEALTH_STATUS", condition=QueryCondition.eq)
	private String healthStatus;

	/**
	 * 性别
	 */
	@TableField("SEX")
	private String sex;
	@TableField(exist = false)
	private String sexName;
	
	/**
	 * 所属部门
	 */
	@TableField("DEPT_ID")
	@QueryField(queryColumn="DEPT_ID", condition=QueryCondition.eq)
	private String deptId;
	@TableField(exist = false)
	private String deptName;

	/**
	 * 宠物编号
	 */
	@TableField("PET_NO")
	private String petNo;
	
	/**
	 * 备注
	 */
	@TableField("MEMO")
	private String memo;

	/**
	 * 出生日期
	 */
	@TableField(value="BIRTHDAY")
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private Date birthday;

	/**
	 * 出生日期字符串
	 */
	@TableField(exist = false)
	private String birthdayStr;

	/**
	 * 品种分类（字典：动物品种）
	 */
	@TableField("BREED")
	private String breed;
	@TableField(exist = false)
	private String breedName;

	/**
	 * 宠物商业分类:1:活体  2：顾客宠物
	 */
	@TableField("BIZ_KIND")
	private String bizKind;
	@TableField(exist = false)
	private String bizKindName;

	/**
	 * 宠物售卖状态 0：正常 1：已售卖
	 */
	@TableField("SALE_STATUS")
	@QueryField(queryColumn="SALE_STATUS", condition=QueryCondition.eq)
	private String saleStatus;

	public String getBirthdayStr() {
		if(this.birthday != null) {
			return DateUtils.getDate2Str(this.birthday);
		}
		return null;
	}
}
