package com.zhonghe.active4j.biz.entity;

/**
 * 宠物信息敞亮
 */
public class PetConstrant {

	// 宠物状态
	// 状态 1:正常 2：生病了 3：死亡
	public static final String PET_HEALTH_STATUS_OK = "1";
	public static final String PET_HEALTH_STATUS_SICK = "2";
	public static final String PET_HEALTH_STATUS_DIE = "3";


}
