package com.zhonghe.active4j.biz.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zhonghe.active4j.biz.entity.PetEntity;
import com.zhonghe.active4j.biz.entity.SupplierEntity;

/**
 * 供应商管理service类
 */
public interface SupplierService extends IService<SupplierEntity> {

    /**
     * 保存供应商
     */
    void saveSupplier(SupplierEntity supplier);

    /**
     * 更新供应商
     */
    void saveOrUpdatePet(SupplierEntity supplier);

    /**
     * 删除供应商
     */
    void delete(SupplierEntity supplier);

}
