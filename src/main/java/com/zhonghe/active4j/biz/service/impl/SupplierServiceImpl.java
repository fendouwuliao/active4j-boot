package com.zhonghe.active4j.biz.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhonghe.active4j.biz.dao.SupplierDao;
import com.zhonghe.active4j.biz.entity.SupplierEntity;
import com.zhonghe.active4j.biz.service.SupplierService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("supplierService")
@Transactional
public class SupplierServiceImpl extends ServiceImpl<SupplierDao, SupplierEntity> implements SupplierService {

    @Override
    public void saveSupplier(SupplierEntity supplier) {
        super.save(supplier);
    }

    @Override
    public void saveOrUpdatePet(SupplierEntity supplier) {
        super.saveOrUpdate(supplier);
    }

    @Override
    public void delete(SupplierEntity supplier) {
        super.removeById(supplier);
    }
}
