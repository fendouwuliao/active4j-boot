package com.zhonghe.active4j.biz.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zhonghe.active4j.biz.entity.PetEntity;

/**
 * 宠物管理service类
 */
public interface PetService extends IService<PetEntity> {

    /**
     * 保存宠物
     */
    void savePet(PetEntity pet);

    /**
     * 更新宠物
     */
    void saveOrUpdatePet(PetEntity pet);

    /**
     * 删除宠物
     */
    void delete(PetEntity pet);

}
