package com.zhonghe.active4j.biz.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhonghe.active4j.biz.dao.PetDao;
import com.zhonghe.active4j.biz.entity.PetEntity;
import com.zhonghe.active4j.biz.service.PetService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("petService")
@Transactional
public class PetServiceImpl extends ServiceImpl<PetDao, PetEntity> implements PetService {

    /**
     * 保存宠物
     */
    @Override
    public void savePet(PetEntity pet) {
        super.save(pet);

    }

    /**
     * 更新宠物
     */
    @Override
    public void saveOrUpdatePet(PetEntity pet) {
        super.saveOrUpdate(pet);
    }

    /**
     * 删除宠物
     */
    @Override
    public void delete(PetEntity pet) {
        super.removeById(pet.getId());
    }

}
