package com.zhonghe.active4j.biz.wrapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.zhonghe.active4j.biz.entity.SupplierEntity;
import com.zhonghe.active4j.common.Wrapper.BaseWrapper;

import java.math.BigDecimal;

public class SupplierWrapper extends BaseWrapper<SupplierEntity> {


    public SupplierWrapper(IPage<SupplierEntity> pageResult) {
        super(pageResult);

        pageResult.getRecords().forEach(d -> {

            BigDecimal yiFuPay = d.getYiFuPay() == null ? BigDecimal.ZERO : d.getYiFuPay();
            BigDecimal yingFuPay = d.getYingFuPay() == null ? BigDecimal.ZERO : d.getYingFuPay();

            d.setYiFuPayStr(yiFuPay.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString());
            d.setYingFuPayStr(yingFuPay.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString());

        });
    }


}
