package com.zhonghe.active4j.biz.wrapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.zhonghe.active4j.biz.entity.PetEntity;
import com.zhonghe.active4j.common.Wrapper.BaseWrapper;
import com.zhonghe.active4j.system.entity.SysUserEntity;
import com.zhonghe.active4j.system.util.SystemUtils;

public class PetWrapper extends BaseWrapper<PetEntity>{


	public PetWrapper(IPage<PetEntity> pageResult) {
		super(pageResult);
		
		pageResult.getRecords().forEach(d -> {
			d.setDeptName(SystemUtils.getDeptNameById(d.getDeptId()));
			d.setBreedName(SystemUtils.getDictionaryValue("animal_breed", d.getBreed()));
			d.setSexName(SystemUtils.getDictionaryValue("common_sex", d.getSex()));
			d.setBizKindName(SystemUtils.getDictionaryValue("pet_biz_kind", d.getBizKind()));
			d.setSaleStatus("1".equals(d.getSaleStatus()) ? "已售卖":"正常");
		});
	}
	
	
	
}
