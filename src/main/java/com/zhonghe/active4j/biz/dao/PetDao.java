package com.zhonghe.active4j.biz.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhonghe.active4j.biz.entity.PetEntity;

public interface PetDao extends BaseMapper<PetEntity> {

}
