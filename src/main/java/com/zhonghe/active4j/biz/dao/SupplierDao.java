package com.zhonghe.active4j.biz.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhonghe.active4j.biz.entity.PetEntity;
import com.zhonghe.active4j.biz.entity.SupplierEntity;

public interface SupplierDao extends BaseMapper<SupplierEntity> {

}
